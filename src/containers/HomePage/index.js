import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router';
import { Row, Column } from 'hedron';

import Title from '../../components/Title';

const HomePage = () => {
  return (
    <Row>
      <Column>
        <Title>
          Salesforce.org Code Demo
        </Title>
        <ul>
          <li>React + Redux</li>
          <li>Authentication via Github</li>
          <li>Two way data binding integrating firebase + re-base with component state</li>
        </ul>
      </Column>
    </Row>
  );
};

export default HomePage;
