import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Row, Column } from 'hedron';
import style from 'styled-components';

import checkAuth from '../../components/requireAuth';
import Title from '../../components/Title';

const ProfileImg = style.picture`
  > img {
    border-radius: 50%;
    width: 10rem;
  }
`;

const AccountPage = ({ user }) => (
  <Row>
    <Column>
      <Title>{user.displayName} ({user.email})</Title>
      <ProfileImg>
        <img src={`${user.photoURL}`} alt={'profile img'} />
      </ProfileImg>
    </Column>
  </Row>
);

AccountPage.propTypes = {
  user: PropTypes.object
};

export default checkAuth(AccountPage);
