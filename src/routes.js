import React from 'react';
import { Route, IndexRoute } from 'react-router';

import HomePage from './containers/HomePage';
import NotFound from './containers/NotFoundPage';
import Layout from './containers/Layout';
import Admin from './containers/AccountPage';

export default function Routes(store) {
  return (
    <Route path='/' component={Layout}>
      <IndexRoute component={HomePage}/>
      <Route path='layout' component={Layout}/>
      <Route path='admin' component={Admin}/>
      <Route path='*' component={NotFound}/>
    </Route>
  );
}
