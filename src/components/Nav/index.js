import styled from 'styled-components';

export default styled.nav`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 2rem;
  width: 100%;

  > * {
    margin: 0 5px 5px 0;
  }
  > *:last-child {
    margin-right: inherit;
  }
`;
